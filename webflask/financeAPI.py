import yfinance as yf
from datetime import datetime

class yfinanceAPI:
    
    def __init__(self, twitteraccount):
        
        if twitteraccount == "unilever":
            self.company = "UL"
        elif twitteraccount == "google":
            self.company = "GOOGL"
        elif twitteraccount == "amazon":
            self.company = "AMZN"
        elif twitteraccount == "tesla":
            self.company = "TSLA"
        elif twitteraccount == "proctergamble":
            self.company = "PG"
        elif twitteraccount == "disney":
            self.company = "DIS"
        elif twitteraccount == "microsoft":
            self.company = "MSFT"
        elif twitteraccount == "walmart":
            self.company = "WMT"
        elif twitteraccount == "intel":
            self.company = "INTC"
        elif twitteraccount == "cvshealth":
            self.company = "CVS"
        else:
            self.company = ""
        
    def getCompany(self):
        return self.company    
        
    def stockPlot(self):
        quote = yf.Ticker(self.company)
        today = datetime.today().strftime('%Y-%m-%d')   
        hist = quote.history(start="2010-01-01",  end=today)
        hist.head()
        toPlot = hist.reset_index()
        for i in ['Open', 'High', 'Close', 'Low']: 
            toPlot[i]  =  toPlot[i].astype('float64')
        return toPlot