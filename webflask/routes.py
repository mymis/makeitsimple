from financeAPI import yfinanceAPI
import os
from connections import make_connection, close_connection
from flask import Flask,render_template, request, url_for, flash, redirect, session, escape
from flask_mysqldb import MySQL
from twitterAPI import TweetAPI
import plotly.express as px
from datetime import datetime
from flask_mail import Message, Mail

app = Flask(__name__)
app.config.from_object('config')
mysql = MySQL(app)
mail = Mail()
mail.init_app(app)

@app.errorhandler(404)
def page_not_found(error):
    return render_template('404.html', title = '404'), 404

@app.errorhandler(500)
def internal_error(error):
    # sending error to admin email
    import logging
    from logging.handlers import SMTPHandler
    from config import ADMINS, MAIL_SERVER, MAIL_PORT, MAIL_USERNAME, MAIL_PASSWORD
    credentials = None
    if MAIL_USERNAME or MAIL_PASSWORD:
        credentials = (MAIL_USERNAME, MAIL_PASSWORD)
    mail_handler = SMTPHandler((MAIL_SERVER, MAIL_PORT), 'no-reply@' + MAIL_SERVER, ADMINS, 'MIS failure', credentials)
    mail_handler.setLevel(logging.ERROR)
    app.logger.addHandler(mail_handler)
    return render_template('500.html', title = '500'), 500

@app.route("/", methods=['GET'])
def index():
    if session.get("userPreference") is None:
        session["userPreference"] = False
    if session.get("loginAttempts") is None:
        session["loginAttempts"] = 0
    if 'userPreference' in request.args and request.args['userPreference'] == 'true':
        session["userPreference"] = True
    if session["userPreference"]:
        return render_template('/index.html', cookies='true')
    else:
        return render_template('/index.html', cookies='false')

@app.route('/about_us')
def about():
    return render_template('aboutUs.html')

@app.route('/contact_us', methods=['GET', 'POST'])
def contact():
    if request.method == 'POST':
        name = escape(request.form['name'])
        email = escape(request.form['email'])
        subject = escape(request.form['subject'])
        message = escape(request.form['message'])
        if len(name) == 0 or len(email) == 0 or len(subject) == 0 or len(message) == 0:
            flash('All fields are required.')
            return render_template('contactUs.html')
        else:
            msg = Message(subject='Contact from MIS: '+subject, sender=email, recipients=app.config.get("ADMINS"))
            msg.body = """
            From: %s <%s>
            %s
            """ % (name, email, message)
            mail.send(msg)
            flash('Message sent. We will return to you shortly')
            return render_template('contactUs.html')
 
    elif request.method == 'GET':
        return render_template('contactUs.html')

@app.route('/your_data')
def yourData():
    return render_template('yourData.html')

@app.route('/faq')
def faq():
    return render_template('faq.html')

@app.route('/loginPage')
def login():
    if "user" in session:
        return redirect(url_for('loggedUser'))
    
    return render_template('loginPage.html')

@app.route('/inside')
def adminlogin():
    if "adminuser" in session:
        return redirect(url_for('loggedInside'))
    
    return render_template('loginAdmin.html')

@app.route("/login/user", methods=['POST', 'GET'])
def loginUser():
    
    if "user" in session:
        return redirect(url_for('loggedUser'))
    
    if request.method == 'GET':
        return redirect(url_for('login'))
     
    if request.method == 'POST':
        session.permanent = False
        # conect database
        con = make_connection(app.config.get("MYSQL_HOST"), app.config.get("MYSQL_USER"), app.config.get("MYSQL_PASSWORD"), app.config.get("MYSQL_DB"))
        # getting data from forms/html/python/etc - PRE-SQL PROCESSING
        email = escape(request.form['email'])
        password = escape(request.form['password'])
        # init cursor
        if session["loginAttempts"] == 3:
            cursor = mysql.connection.cursor()
            cursor.callproc('find_email', [email,])
            if cursor.rowcount != 0:
                cursor.close()
                cursor = mysql.connection.cursor()
                cursor.callproc('block_user', [email,])
                mysql.connection.commit()
                cursor.close()
                session["loginAttempts"] = 0
                message = "User "+email+" was blocked while trying to login. Please help him with his access."
                msg = Message(subject='Blocked user', sender=email, recipients=app.config.get("ADMINS"))
                msg.body = """
                From: %s <%s>
                %s
                """ % (email, email, message)
                mail.send(msg)
                flash("Excessive login attempts. Your user is blocked. Please contact us to unblock", "info")
            else:
                cursor.close()
                flash("Excessive login attempts. Your session is blocked. Please contact us to unblock", "info")
            close_connection(con)
            return render_template('loginPage.html')
        cursor = mysql.connection.cursor()
        # sql code for mysql
        cursor.callproc('login_user', [email,password])

        rowsCount = cursor.rowcount
        rows = cursor.fetchone()
        # cursor close
        cursor.close()
        # connection close
        close_connection(con)
        if rowsCount == 0:
            flash("Invalid login: wrong e-mail or password. Please try again", "info")
            session["loginAttempts"] += 1
            return render_template('loginPage.html')
        # for row in rows:
        if rows[4] == 1:
            session["loginAttempts"] == 0
            flash("Excessive login attempts. Your user is blocked. Please contact us to unblock", "info")
            return render_template('loginPage.html')
        session["user"] = str(rows[2])
        session["name"] = str(rows[1])
        session["id"] = str(rows[0])
        session["loginAttempts"] = 0
        return redirect(url_for('loggedUser'))

@app.route("/inside/user", methods=['POST', 'GET'])
def loginInside():
    
    if "adminuser" in session:
        return redirect(url_for('loggedInside'))
    
    if request.method == 'GET':
        return redirect(url_for('adminlogin'))
     
    if request.method == 'POST':
        session.permanent = False
        # conect database
        con = make_connection(app.config.get("MYSQL_HOST"), app.config.get("MYSQL_USER"), app.config.get("MYSQL_PASSWORD"), app.config.get("MYSQL_DB"))
        # getting data from forms/html/python/etc - PRE-SQL PROCESSING
        email = escape(request.form['email'])
        password = escape(request.form['password'])
        # init cursor
        cursor = mysql.connection.cursor()
        # sql code for mysql
        cursor.callproc('login_inside', [email,password])
        rowsCount = cursor.rowcount
        rows = cursor.fetchone()
        # cursor close
        cursor.close()
        # connection close
        close_connection(con)
        if rowsCount == 0:
            flash("Invalid login: wrong e-mail or password. Please try again", "info")
            return render_template('loginAdmin.html')
        session["adminuser"] = str(rows[2])
        session["adminname"] = str(rows[1])
        session["adminid"] = str(rows[0])
        return redirect(url_for('loggedInside'))

@app.route('/changePassword')
def changePassword():
    if "user" in session:
        return render_template('changePassword.html')
    
    return render_template('loginPage.html')

@app.route("/login/change", methods=['POST', 'GET'])
def changeUserPassword():
    if request.method == 'GET':
        return redirect(url_for('/'))
    if "user" in session:
        user = session["user"]
        name = session["name"]
        id = session["id"]
        password = escape(request.form['password'])
        retypepassword = escape(request.form['retypepassword'])
        if len(password) < 8:
            flash("Password must have at least 8 characters", "info")
            return render_template('changePassword.html')
        if password != retypepassword:
            flash("Password must be equal in both fields", "info")
            return render_template('changePassword.html')
        con = make_connection(app.config.get("MYSQL_HOST"), app.config.get("MYSQL_USER"), app.config.get("MYSQL_PASSWORD"), app.config.get("MYSQL_DB"))
        # init cursor
        cursor = mysql.connection.cursor()
        # sql code for mysql
        cursor.callproc('update_pass', [password, id])
        # comit cursor when INSERT/DELETE/UPDATE
        mysql.connection.commit()
        # cursor close
        cursor.close()
        # connection close
        close_connection(con)
        flash(f"You have successfully changed your password, {name}!", "info")
        return render_template('loggedArea.html', user=user)
    else:
        return redirect(url_for('login'))

@app.route('/signupform')
def form():
    if "user" in session:
        return redirect(url_for('loggedUser'))
    
    return render_template('signupform.html')

@app.route("/register/user", methods=['POST', 'GET'])
def registerUser():
    
    if "user" in session:
        return redirect(url_for('loggedUser'))
    
    if request.method == 'GET':
        return render_template('signupform.html')
     
    if request.method == 'POST':
        # getting data from forms/html/python/etc - PRE-SQL PROCESSING
        name = escape(request.form['name'])
        email = escape(request.form['email'])
        password = escape(request.form['password'])
        retypepassword = escape(request.form['retypepassword'])
        if len(password) < 8:
            flash("Password must have at least 8 characters", "info")
            return render_template('signupform.html')
        if password != retypepassword:
            flash("Password must be equal in both fields", "info")
            return render_template('signupform.html')
        
        con = make_connection(app.config.get("MYSQL_HOST"), app.config.get("MYSQL_USER"), app.config.get("MYSQL_PASSWORD"), app.config.get("MYSQL_DB"))
        # getting data from forms/html/python/etc - PRE-SQL PROCESSING
        # init cursor
        cursor = mysql.connection.cursor()
        # sql code for mysql
        cursor.callproc('find_email', [email,])
        countEmail = cursor.rowcount
        # cursor close
        cursor.close()
        #connection close
        close_connection(con)
        
        if countEmail > 0:
            flash("This e-mail already exists in the system. Try a different one or login", "info")
            return render_template('signupform.html')
        
        con = make_connection(app.config.get("MYSQL_HOST"), app.config.get("MYSQL_USER"), app.config.get("MYSQL_PASSWORD"), app.config.get("MYSQL_DB"))
        # init cursor
        cursor = mysql.connection.cursor()
        # sql code for mysql
        cursor.callproc('create_user', [name,email,password])
        # comit cursor when INSERT/DELETE/UPDATE
        mysql.connection.commit()
        # cursor close
        cursor.close()
        # connection close
        close_connection(con)
        flash("User sucessfully created. Now you can login!", "info")
        return redirect(url_for('login'))
    
@app.route("/logged/user", methods=['POST', 'GET'])
def loggedUser():
    if "user" in session:
        user = session["user"]
        name = session["name"]
        flash(f"You have successfully logged in, {name}!", "info")
        return render_template('loggedArea.html', user=user)
    else:
        return redirect(url_for('login'))
    
@app.route("/logged/inside", methods=['POST', 'GET'])
def loggedInside():
    if "adminuser" in session:
        adminuser = session["adminuser"]
        adminname = session["adminname"]
        flash(f"You have successfully logged in, {adminname}!", "info")
        return render_template('loggedAdmin.html', adminuser=adminuser)
    else:
        return redirect(url_for('loginInside'))
    
@app.route("/deleteUser", methods=['POST', 'GET'])
def deleteUser():
    if "adminuser" in session:
        adminuser = session["adminuser"]
        adminname = session["adminname"]
        email = escape(request.form['email'])
        con = make_connection(app.config.get("MYSQL_HOST"), app.config.get("MYSQL_USER"), app.config.get("MYSQL_PASSWORD"), app.config.get("MYSQL_DB"))
        # getting data from forms/html/python/etc - PRE-SQL PROCESSING
        # init cursor
        cursor = mysql.connection.cursor()
        # sql code for mysql
        cursor.callproc('find_email', [email,])
        countEmail = cursor.rowcount
        countuser = cursor.fetchone()
        # cursor close
        cursor.close()
        # connection close
        close_connection(con)
        
        if countEmail == 0:
            flash(f"This user doesn't exists in the system. Try a different one, {adminname}!", "info")
            return render_template('loggedAdmin.html', adminuser=adminuser)
        
        id = countuser [0]
        con = make_connection(app.config.get("MYSQL_HOST"), app.config.get("MYSQL_USER"), app.config.get("MYSQL_PASSWORD"), app.config.get("MYSQL_DB"))
        # init cursor
        cursor = mysql.connection.cursor()
        # sql code for mysql
        cursor.callproc('delete_user', [id,])
        # comit cursor when INSERT/DELETE/UPDATE
        mysql.connection.commit()
        # cursor close
        cursor.close()
        # connection close
        close_connection(con)
        flash(f"User {email} successfylly deleted, {adminname}!", "info")
        return render_template('loggedAdmin.html', adminuser=adminuser)
    else:
        return redirect(url_for('loginInside'))
    
@app.route("/unblockUser", methods=['POST', 'GET'])
def unblockUser():
    if "adminuser" in session:
        adminuser = session["adminuser"]
        adminname = session["adminname"]
        email = escape(request.form['email'])
        con = make_connection(app.config.get("MYSQL_HOST"), app.config.get("MYSQL_USER"), app.config.get("MYSQL_PASSWORD"), app.config.get("MYSQL_DB"))
        # getting data from forms/html/python/etc - PRE-SQL PROCESSING
        # init cursor
        cursor = mysql.connection.cursor()
        # sql code for mysql
        cursor.callproc('find_email', [email,])
        countEmail = cursor.rowcount
        countuser = cursor.fetchone()
        # cursor close
        cursor.close()
        # connection close
        close_connection(con)
        
        if countEmail == 0:
            flash(f"This user doesn't exists in the system. Try a different one, {adminname}!", "info")
            return render_template('loggedAdmin.html', adminuser=adminuser)
        
        id = countuser [0]
        con = make_connection(app.config.get("MYSQL_HOST"), app.config.get("MYSQL_USER"), app.config.get("MYSQL_PASSWORD"), app.config.get("MYSQL_DB"))
        # init cursor
        cursor = mysql.connection.cursor()
        # sql code for mysql
        cursor.callproc('unblock_user', [id,])
        # comit cursor when INSERT/DELETE/UPDATE
        mysql.connection.commit()
        # cursor close
        cursor.close()
        # connection close
        close_connection(con)
        flash(f"User {email} successfylly unblocked, {adminname}!", "info")
        return render_template('loggedAdmin.html', adminuser=adminuser)
    else:
        return redirect(url_for('loginInside'))
    
@app.route('/dashboardChoice')
def dashboardChoice():
    if "user" in session:
        user = session["user"]
        name = session["name"]
        if  "trendCompany" in session: # somebody left Trends result without saving
            session.pop("trendCompany", None)
            session.pop("barTwitter", None)
            session.pop("plotY", None)
            session.pop("TwitterP", None)
            session.pop("TwitterO", None)
        flash(f"Hi {name},", "info")
        return render_template('dashboardChoice.html', user=user)
    else:
        return redirect(url_for('login'))
    
@app.route('/dashboardChoosen', methods=['POST', 'GET'])
def dashboardChoosen():
    if request.method == 'GET':
        return redirect(url_for('dashboardChoice'))
    if request.method == 'POST':
        twitterAccount = request.form.get("twitterAccount")
        if "user" in session: 
            tweet = TweetAPI(twitterAccount)
            trends = tweet.getTrends()
            yfinance = yfinanceAPI(twitterAccount)
            toPlot = yfinance.stockPlot()
            toPlotTitle = yfinance.getCompany() + " Stock Prices"
            nameBarTitle = twitterAccount + " Twitter Trends"
            user = session["user"]
            name = session["name"]
            fig2 = px.bar(x=trends[2], y=trends[3], labels={'x':'Sentiment', 'y':'Amount'}, title=nameBarTitle, width=600, height=500)
            nameBar = "./static/images/bar" + name + datetime.today().strftime('%Y-%m-%d-%H-%M-%S') + ".png"
            namePlot = "./static/images/plot" + name + datetime.today().strftime('%Y-%m-%d-%H-%M-%S') + ".png"
            fig2.write_image(nameBar)
            fig = px.line(toPlot, x="Date", y="Open", title=toPlotTitle, width=600, height=500)
            fig.write_image(namePlot)
            session["trendCompany"] = twitterAccount
            session["barTwitter"] = nameBar
            session["plotY"] = namePlot
            session["TwitterP"] = trends[0]
            session["TwitterO"] = trends[1]
            flash(f"Hi, {name}! That's the graphics for the choosen company.", "info")
            return render_template('dashboardChoosen.html', user=user, trends=trends, twitterAccount=twitterAccount, bar=nameBar, plot=namePlot)
        else:
            return redirect(url_for('login'))
        
@app.route("/saveTrends")
def saveTrends():
    if "user" in session and "trendCompany" in session:
        user = session["user"]
        name = session["name"]
        id = session["id"]
        trendCompany = session["trendCompany"]
        barTwitter = session["barTwitter"]
        plotY = session["plotY"]
        TwitterP = session["TwitterP"]
        TwitterO = session["TwitterO"]
        date_time = datetime.today().strftime('%Y-%m-%d-%H:%M:%S')
        # conect database
        con = make_connection(app.config.get("MYSQL_HOST"), app.config.get("MYSQL_USER"), app.config.get("MYSQL_PASSWORD"), app.config.get("MYSQL_DB"))
        # init cursor
        cursor = mysql.connection.cursor()
        # sql code for mysql
        cursor.callproc('save_trend', [id, trendCompany, date_time, barTwitter, plotY, TwitterP, TwitterO])
        # comit cursor when INSERT/DELETE/UPDATE
        mysql.connection.commit()
        # cursor close
        cursor.close()
        # connection close
        close_connection(con)
        session.pop("trendCompany", None)
        session.pop("barTwitter", None)
        session.pop("plotY", None)
        session.pop("TwitterP", None)
        session.pop("TwitterO", None)
        flash(f"You have saved Trends successfully, {name}", "info")
        return render_template('dashboardChoice.html', user=user)
    else:
        return redirect(url_for('login'))
    
@app.route("/dashboardBack")
def dashboardBack():
    if "user" in session and "trendCompany" in session:
        user = session["user"]
        name = session["name"]
        os.remove(session["barTwitter"])
        os.remove(session["plotY"])
        session.pop("trendCompany", None)
        session.pop("barTwitter", None)
        session.pop("plotY", None)
        session.pop("TwitterP", None)
        session.pop("TwitterO", None)
        flash(f"Welcome back, {name}", "info")
        return render_template('dashboardChoice.html', user=user)
    else:
        return redirect(url_for('login')) 


@app.route('/educational')
def educational():
    if "user" in session:
        user = session["user"]
        name = session["name"]
        flash(f"Hi, {name}!", "info")
        return render_template('educational.html', user=user)
    else:
        return redirect(url_for('login'))
    

@app.route('/financialPlanning')
def financialPlanning():
    if "user" in session:
        user = session["user"]
        name = session["name"]
        flash(f"Hi, {name}!", "info")
        return render_template('financialPlanning.html', user=user)
    else:
        return redirect(url_for('login'))

@app.route('/ridOfDebts')
def ridOfDebts():
    if "user" in session:
        user = session["user"]
        name = session["name"]
        flash(f"Hi, {name}!", "info")
        return render_template('ridOfDebts.html', user=user)
    else:
        return redirect(url_for('login'))

@app.route('/investorProfile')
def investorProfile():
    if "user" in session:
        user = session["user"]
        name = session["name"]
        flash(f"Hi, {name}!", "info")
        return render_template('investorProfile.html', user=user)
    else:
        return redirect(url_for('login'))

@app.route('/emergencyReservation')
def emergencyReservation():
    if "user" in session:
        user = session["user"]
        name = session["name"]
        flash(f"Hi, {name}!", "info")
        return render_template('emergencyReservation.html', user=user)
    else:
        return redirect(url_for('login'))

@app.route('/patienteDiscipline')
def patienteDiscipline():
    if "user" in session:
        user = session["user"]
        name = session["name"]
        flash(f"Hi, {name}!", "info")
        return render_template('patienteDiscipline.html', user=user)
    else:
        return redirect(url_for('login'))

@app.route('/moneyInvestments')
def moneyInvestments():
    if "user" in session:
        user = session["user"]
        name = session["name"]
        flash(f"Hi, {name}!", "info")
        return render_template('moneyInvestments.html', user=user)
    else:
        return redirect(url_for('login'))

@app.route('/pointsToConsider')
def pointsToConsider():
    if "user" in session:
        user = session["user"]
        name = session["name"]
        flash(f"Hi, {name}!", "info")
        return render_template('pointsToConsider.html', user=user)
    else:
        return redirect(url_for('login'))

@app.route('/savedChoices')
def savedChoices():
    if "user" in session:
        if "trendCompany" in session:
            session.pop("trendCompany", None)
            session.pop("barTwitter", None)
            session.pop("plotY", None)
            session.pop("TwitterP", None)
            session.pop("TwitterO", None)
        con = make_connection(app.config.get("MYSQL_HOST"), app.config.get("MYSQL_USER"), app.config.get("MYSQL_PASSWORD"), app.config.get("MYSQL_DB"))
        # getting data from forms/html/python/etc - PRE-SQL PROCESSING
        id_user = session["id"]
        # init cursor
        cursor = mysql.connection.cursor()
        # sql code for mysql
        cursor.callproc('show_trends', [id_user,])
        countTrends = cursor.rowcount
        savedTrends = cursor.fetchall()
        # cursor close
        cursor.close()
        # connection close
        close_connection(con)
        if countTrends == 0:
            flash(f"You don't have any saved trends, please choose one new.", "info")
            return render_template('dashboardChoice.html', user=session["user"])
    
        return render_template('savedChoices.html', savedTrends=savedTrends)
    else:
        return redirect(url_for('login'))

@app.route('/savedChoose', methods=['GET'])
def savedChoose():
    if not 'idTrend' in request.args:
        return redirect(url_for('savedChoices'))
    idTrend = request.args['idTrend']
    if "user" in session:
        user = session["user"]
        name = session["name"]
        if "trendCompany" in session:
            session.pop("trendCompany", None)
            session.pop("barTwitter", None)
            session.pop("plotY", None)
            session.pop("TwitterP", None)
            session.pop("TwitterO", None)
        con = make_connection(app.config.get("MYSQL_HOST"), app.config.get("MYSQL_USER"), app.config.get("MYSQL_PASSWORD"), app.config.get("MYSQL_DB"))
        # getting data from forms/html/python/etc - PRE-SQL PROCESSING
        # init cursor
        cursor = mysql.connection.cursor()
        # sql code for mysql
        cursor.callproc('select_trend', [idTrend,])
        savedTrends = cursor.fetchone()
        #cursor close
        cursor.close()
        # connection close
        close_connection(con)
        twitterAccount = savedTrends[2]
        trends = [savedTrends[6], savedTrends[7]]
        nameBar = savedTrends[4]
        namePlot = savedTrends[5]
        session["trendCompany"] = twitterAccount
        session["barTwitter"] = nameBar
        session["plotY"] = namePlot
        session["TwitterP"] = trends[0]
        session["TwitterO"] = trends[1]
        flash(f"Hi, {name}! That's the graphics for the choosen company.", "info")
        return render_template('savedChoose.html', user=user, trends=trends, twitterAccount=twitterAccount, bar=nameBar, plot=namePlot, idTrend=idTrend)
    else:
        return redirect(url_for('login'))
    
@app.route("/deleteTrends", methods=['GET'])
def deleteTrends():
    if not 'idTrend' in request.args:
        return redirect(url_for('savedChoices'))
    idTrend = request.args['idTrend']
    if "user" in session and "trendCompany" in session:
        user = session["user"]
        name = session["name"]
        # connect database
        con = make_connection(app.config.get("MYSQL_HOST"), app.config.get("MYSQL_USER"), app.config.get("MYSQL_PASSWORD"), app.config.get("MYSQL_DB"))
        # init cursor
        cursor = mysql.connection.cursor()
        # sql code for mysql

        cursor.callproc('delete_trend', [idTrend,])
        # comit cursor when INSERT/DELETE/UPDATE
        mysql.connection.commit()
        # cursor close
        cursor.close()
        # connection close
        close_connection(con)
        os.remove(session["barTwitter"])
        os.remove(session["plotY"])
        session.pop("trendCompany", None)
        session.pop("barTwitter", None)
        session.pop("plotY", None)
        session.pop("TwitterP", None)
        session.pop("TwitterO", None)
        flash(f"You have deleted Trends successfully, {name}", "info")
        return render_template('dashboardChoice.html', user=user)
    else:
        return redirect(url_for('login'))
    
@app.route("/savedBack")
def savedBack():
    if "user" in session and "trendCompany" in session:
        user = session["user"]
        name = session["name"]
        session.pop("trendCompany", None)
        session.pop("barTwitter", None)
        session.pop("plotY", None)
        session.pop("TwitterP", None)
        session.pop("TwitterO", None)
        flash(f"Welcome back, {name}", "info")
        return render_template('dashboardChoice.html', user=user)
    else:
        return redirect(url_for('login')) 
    
@app.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('index'))

@app.route('/logoutInside')
def logoutInside():
    session.clear()
    return redirect(url_for('index'))

if __name__ == "__main__":
    app.secret_key = 'super secret key'
    app.config['SESSION_TYPE'] = 'filesystem'


    app.debug = True
    app.run()