import tweepy
from textblob import TextBlob
import pandas as pd
import numpy as np
import re
# import matplotlib.pyplot as plt
from pylab import rcParams

class TweetAPI:
    rcParams ['figure.figsize'] = 8, 6

    # Let's load the config file (from: twitter Dev in: Twitter API details).
    config = pd.read_csv("./config.csv")

    # Twiiter API config - NEED TO GET IT
    twitterApiKey = config['twitterApiKey'][0]
    twitterApiSecret = config['twitterApiSecret'][0]
    twitterApiAccessToken = config['twitterApiAccessToken'][0]
    twitterApiAccessTokenSecret = config['twitterApiAccessTokenSecret'][0]

    # Authenticate
    auth = tweepy.OAuthHandler(twitterApiKey, twitterApiSecret)
    auth.set_access_token(twitterApiAccessToken, twitterApiAccessTokenSecret)
    twitterApi = tweepy.API(auth, wait_on_rate_limit=True)

    def __init__(self, twitterAccount):
        
        self.twitterAccount = twitterAccount
    
    def cleanUpTweet(self, txt):
        # Remove mentions
        txt = re.sub(r'@[A-Za-z0-9_]+', '', txt)
        # Remove hashtags
        txt = re.sub(r'#', '', txt)
        # Remove retweets:
        txt = re.sub(r'RT : ', '', txt)
        # Remove urls
        txt = re.sub(r'https?:\/\/[A-Za-z0-9\.\/]+', '', txt)
        return txt
    
    # functions to calculate the subjectivity and polarity of our tweets
    def getTextSubjectivity(self, txt):
        return TextBlob(txt).sentiment.subjectivity

    def getTextPolarity(self, txt):
        return TextBlob(txt).sentiment.polarity
    
    # negative, nautral, positive analysis
    def getTextAnalysis(self, a):
        if a < 0:
            return "Negative"
        elif a == 0:
            return "Neutral"
        else:
            return "Positive"

    def getTrends(self):
       # Now we are going to retrieve the last 50 Tweets & replies from the specified Twitter account.
        tweets = tweepy.Cursor(self.twitterApi.user_timeline, 
                        screen_name=self.twitterAccount, 
                        count=None,
                        since_id=None,
                        max_id=None,
                        trim_user=True,
                        exclude_replies=True,
                        contributor_details=False,
                        include_entities=False
                        ).items(500);

        # And we are going to create Pandas Data Frame from it.
        df = pd.DataFrame(data=[tweet.text for tweet in tweets], columns=['Tweet'])

        # Let's see what is in the data frame by calling the head() function.
        df.head()
        
        # And now we are going to apply it for all the Tweets in our Pandas Data Frame.
        df['Tweet'] = df['Tweet'].apply(self.cleanUpTweet)
        
        # apply these functions to our data frame and create two new features in our data frame Subjectivity and Polarity.
        df['Subjectivity'] = df['Tweet'].apply(self.getTextSubjectivity)
        df['Polarity'] = df['Tweet'].apply(self.getTextPolarity)

        df.head(50)
        
        # And apply this function and create another feature in our data frame called Score.
        df['Score'] = df['Polarity'].apply(self.getTextAnalysis)
        
        # OUTPUT: data frame with our Tweets, Subjectivity, Polarity and Score for all our Tweets.
        df.head(50)

        positive = df[df['Score'] == 'Positive']
        
        positivePrint = str(positive.shape[0]/(df.shape[0])*100) + " % of positive tweets"
        
        labels = df.groupby('Score').count().index.values

        values = df.groupby('Score').size().values
        
        # calculate the percentage of objective tweets.
        objective = df[df['Subjectivity'] == 0]

        objectivePrint = str(objective.shape[0]/(df.shape[0])*100) + " % of objective tweets"
        
        trends = [positivePrint, objectivePrint, labels, values]
        
        return trends
    
        
        